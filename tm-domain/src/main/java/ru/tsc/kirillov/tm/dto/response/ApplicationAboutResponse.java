package ru.tsc.kirillov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class ApplicationAboutResponse extends AbstractResponse {

    @NotNull
    private String email;

    @NotNull
    private String name;

}
